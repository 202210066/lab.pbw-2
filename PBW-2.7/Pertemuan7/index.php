<html>

<head>
    <title>PHP Test</title>
</head>

<body>
    <?php
    $panjang = 5;
    $lebar = 10;
    $IPP = $panjang * $lebar;

    echo $IPP;
    echo "<br>";
    //$1PP = $1PP+2;
    $IPP += 2;
    $IPP -= 2;
    echo $IPP;
    echo "<br>";
    echo $x = 10;
    echo "<br>";
    echo $x == 10;
    echo "<br>";
    var_dump(true && false);
    echo "<br>";
    var_dump(true || false);
    echo "<br>";
    var_dump(false || true);
    echo "<br>";
    var_dump(false && true);
    echo "<br>";
    var_dump(false && false);
    echo "<br>";


    // looping if else
    $hari = "senin";
    if ($hari == "senin") {
        echo "hari pertama kerja <br>";
    } else {
        echo " Bukan hari senin <br>";
    }

    // ternary
    echo ($hari == "senin") ? "I love monday <br>" : "Bukan Hari Senin <br>";
    // echo "<hr>";
    // function cetak($nama)
    // {
    //     echo "hello $nama <br>";
    // }
    // cetak("Dansa");

    // echo "<hr>";
    // function cetak($nama1,$nama2)
    // {
    //     echo "hello $nama1 dan $nama2 <br>";
    // }
    // cetak("Dansa","Denis")

    // function
    // function cetak()
    // {
    //     echo "hello world 1 <br>";    
    //     echo "hello world 2 <br>";
    //     echo "hello world 3 <br>";
    // }

    // cetak();
    // cetak();
    // cetak();
    // cetak();

    function luas($angka1, $angka2 = NULL)
    {
        if ($angka2 == NULL) {
            $luas = $angka1 * $angka1;
        } else {
            $luas = $angka1 * $angka2;
        }
        return $luas;
    }
    echo luas(5, 2);

    // looping
    // for($x = 1; $x < 5 ; $x++)
    // {
    //     echo "hello world! <br>";
    // }

    // "<br>";

    $y = 1;
    while ($y < 5) {
        echo "hello world! <br>";
        $y++;
    }

    echo "<br>";

    $z = 1;
    do {
        echo "hello world <br>";
        $z++;
    } while ($z < 5);

    echo "<hr>";

    $mahasiswa = array("Andrian", "Beta", "Cintya", "Dimas");
    for ($n = 0; $n < 4; $n++) {
        echo $mahasiswa[$n] . "<br>";
    }
    echo $mahasiswa[0];

    echo "<hr>";
    foreach ($mahasiswa as $person) {
        echo "$person <br>";
    }
    print_r($mahasiswa);

    echo "<hr>";
    $age = array("risky" => 20, "lutfi" => 18, "linda" => 21, "johan" => 22);
    foreach ($age as $data => $val) {
        echo "$data = $val <br>";
    }
    ?>
</body>

</html>