var kalimat = "Hello World"; //string 
var angka = " 25"; //integer
var is_trus = true; //boolean
var decimal = 1.2; //double
var motor = ["Yamaha", "Honda", "Suzuki"];
var biodata = {
    first_name: "rezky",
    last_name: "putra",
    age: 24
}; //object

//Huruf Kapital
var kalimat = "Selamat Datang";
var output = kalimat.toUpperCase();
console.log(output);

//Huruf Kecil
var output2 = kalimat.toLowerCase();
console.log(output2);

//Menghitung jumlah string abjad dimulai dari 0
var abjad = "ABCDEFGHIJKLMNOPQRSTUVWXZ";
var jumlah = abjad.length;
console.log(jumlah);

//Menjulahkan string
function penjumlahan(angka1, angka2) {
    return angka1 + angka2;
}
console.log(penjumlahan(4, 3));

// document.getElementById("demo").innerHTML = penjumlahan(12,5);

function bio(nama, umur) {
    return "nama saya" + nama + "umur saya" + umur;

}
document.getElementById("demo").innerHTML = bio("Lutfi", 30);


function hobi(berkebun, berenang, membaca) {
    return "hobi saya : " + berkebun + berenang + membaca;
}

document.getElementById("demo").innerHTML = hobi("berkebun, ", "berenang, ", "membaca, ");

var age = 9;
var ktp = false;
if (age >= 17 && ktp) {
    console.log("Sudah bisa memiliki KTP");
} else {
    console.log("belum bisa memiliki SIM");
}

// looping

var output3 = "";
for (i = 1; i < 10; i++) {
    output3 += i + "-urutan 1 sampai 10 <br>";
}
console.log(output3);

// var index;
// for (let index = 0; index < bound)

document.getElementById("demo").innerHTML = output3;

var i = 1;
var text = "";
while (i <= 10) {
    text += i + "urutan ke-" + i + "<br>"
    i++;
}
document.getElementById("demo").innerHTML = text;

var i = 10;
var text = "";
while (i >= 1) {
    text += i + "IBIK Kesatuan" + i + "<br>";
    i--;
}
document.getElementById("demo").innerHTML = text;