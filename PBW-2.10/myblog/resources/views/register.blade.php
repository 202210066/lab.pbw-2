<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <div class="judul">
        <h2>Buat Account Baru!</h2>
        <h4>Sign Up Form</h4>
    </div>
    <div class="form-signUp">
        <form action="">
            <label for="fname">First Name :</label>
            <input type="text" />
        </form>
        <br />
        <form action="">
            <label for="lname">Last Name :</label>
            <input type="text" />
        </form>
    </div>
    <br />
    <div class="gender">
        <p>Gender</p>
        <form action="">
            <li>
                <input type="radio" />
                <label for="Male">Male</label>
            </li>
            <li>
                <input type="radio" />
                <label for="Female">Female</label>
            </li>
            <li>
                <input type="radio" />
                <label for="Other">Other</label>
            </li>
        </form>
    </div>
    <br />
    <div class="country">
        <p>Nationality</p>
        <form action="">
            <select name="lang" id="">
                <option value="indo">Indonesian</option>
                <option value="indo">English</option>
                <option value="indo">Other</option>
            </select>
        </form>
    </div>
    <div class="lang">
        <p>Language Spoken</p>

        <form action="">
            <li>
                <input type="radio" />
                <label for="indo">Bahasa Indonesia</label>
            </li>
            <li>
                <input type="radio" />
                <label for="eng">English</label>
            </li>
            <li>
                <input type="radio" />
                <label for="Other">Other</label>
            </li>
        </form>
    </div>
    <br />
    <div class="bio">
        <p>Bio :</p>
        <textarea> </textarea>
    </div>
    <br />
    <div class="buttonSignUp">
        <button><a href="/welcome">Sign Up</a></button>
    </div>
</body>

</html>
